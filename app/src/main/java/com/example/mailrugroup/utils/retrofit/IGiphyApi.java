package com.example.mailrugroup.utils.retrofit;

import com.example.mailrugroup.model.data.GiphyListResponse;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Интерфейс, описывающий нужную нам абстракцию api.giphy.com
 */
public interface IGiphyApi {

    @GET("v1/gifs/search")
    public Observable<GiphyListResponse> getListData( @Query("q") String query,
                                                      @Query("api_key") String apiKey );

    @GET("v1/gifs/search")
    public Observable<GiphyListResponse> getListData( @Query("q") String query,
                                                      @Query("offset") String offset,
                                                      @Query("api_key") String apiKey );
}
