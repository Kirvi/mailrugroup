package com.example.mailrugroup.model.data;

import java.io.Serializable;

public abstract class BaseImage implements Serializable {
    public abstract String getUrl();
    public abstract String getWidth();
    public abstract String getHeight();
}
