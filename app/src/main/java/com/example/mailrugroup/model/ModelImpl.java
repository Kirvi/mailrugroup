package com.example.mailrugroup.model;

import com.example.mailrugroup.model.data.GiphyListResponse;
import com.example.mailrugroup.utils.retrofit.GiphyApi;
import com.example.mailrugroup.utils.retrofit.IGiphyApi;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ModelImpl implements IModel {

    /**
     * Ключ для использования в Giphy API.
     */
    private static final String KEY_API = "dc6zaTOxFJmzC";
    private final IGiphyApi mGiphyApi;

    public ModelImpl() {
        mGiphyApi = GiphyApi.getGiphyApi();
    }

    @Override
    public Observable<GiphyListResponse> getListData( final String query ) {
        return mGiphyApi.getListData( query, KEY_API )
                .subscribeOn( Schedulers.io() )
                .observeOn( AndroidSchedulers.mainThread() );
    }

    @Override
    public Observable<GiphyListResponse> getListData(String query, String offset) {
        return mGiphyApi.getListData( query, offset, KEY_API )
                .subscribeOn( Schedulers.io() )
                .observeOn( AndroidSchedulers.mainThread() );
    }
}
