package com.example.mailrugroup.model;

import com.example.mailrugroup.model.data.GiphyListResponse;

import rx.Observable;
/**
 * Интерфейс для работы с уровнем модели.
 * Модель загружает данные из интернета.
 * Догрузка данных происходит по необходимости и обрабатывается в  Presenter.
 */
public interface IModel {

    /**
     * Получить список gif.
     * @param query тематика получаеммых данных.
     */
    public Observable<GiphyListResponse> getListData( final String query );

    /**
     * Получить список gif со смещением.
     * @param query тематика получаеммых данных.
     * @param offset смещение по количеству gif  в списке.
     */
    public Observable<GiphyListResponse> getListData( final String query, final String offset );
}
