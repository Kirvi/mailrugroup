package com.example.mailrugroup.presenter;

import android.content.Context;
import android.os.Bundle;
import android.widget.ImageView;

import com.example.mailrugroup.model.IModel;
import com.example.mailrugroup.model.ModelImpl;
import com.example.mailrugroup.model.data.BaseImage;
import com.example.mailrugroup.model.data.Datum;
import com.example.mailrugroup.model.data.GiphyListResponse;
import com.example.mailrugroup.view.IView;
import com.koushikdutta.ion.Ion;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.functions.Func1;
import rx.subscriptions.Subscriptions;

public class Presenter implements IPresenter {

    private static final String LIST_IMAGE = "com.example.mailrugroup.presenter.list";
    private static final String QUERY = "com.example.mailrugroup.presenter.query";
    private final IView mView;
    private final IModel mModel;
    private Subscription mSubscription = Subscriptions.empty();
    private String mQuery;
    private List<BaseImage> mDataList = Collections.synchronizedList( new ArrayList<BaseImage>() );

    public Presenter( final IView view ) {
        mView = view;
        mModel = new ModelImpl();
    }

    @Override
    public void onCreate( final Bundle savedInstanceState) {
        if( savedInstanceState != null ) {
            mQuery = savedInstanceState.getString( QUERY );
            final List<BaseImage> datas =
                    (List<BaseImage>) savedInstanceState.getSerializable( LIST_IMAGE );
            if( datas != null ) {
                mDataList.clear();
                mDataList.addAll( datas );
                mView.updateData( mDataList );
            }
        } else if( isListEmpty() ) {
            //Приложение запущено впервые: используем запрос по умолчанию для первичной загрузки данных.
            onLoadClick();
        }

    }

    @Override
    public void onSaveInstanceState( final Bundle outState ) {
        if( !isListEmpty() ) {
            outState.putSerializable(LIST_IMAGE, new ArrayList<>(mDataList));
        }
        outState.putString( QUERY, mQuery );
    }

    @Override
    public void onLoadClick() {
        if( !mSubscription.isUnsubscribed() ) {
            mSubscription.unsubscribe();
        }
        mQuery = mView.getQuery();
        mSubscription = mModel.getListData( mQuery )
                .flatMap( new Func1<GiphyListResponse, Observable<List<Datum>>>() {
                    @Override
                    public Observable<List<Datum>> call( final GiphyListResponse giphyListResponse ) {
                        return Observable.just( giphyListResponse.getData() );
                    }
                })
                .flatMap( new Func1<List<Datum>, Observable<List<BaseImage>>>() {
                    @Override
                    public Observable<List<BaseImage>> call( final List<Datum> datums ) {
                        // 25 для оптимизации. Загрузка происходит по 25 элементов.
                        final List<BaseImage> datas = new ArrayList<BaseImage>(25);
                        for( final Datum datum : datums ) {
                            datas.add( datum.getImages().getOriginal() );
                        }
                        return Observable.just( datas );
                    }
                })
                .subscribe( new Observer<List<BaseImage>>() {
                    @Override
                    public void onCompleted() {
                        //do nothing
                    }

                    @Override
                    public void onError( final Throwable e ) {
                        mView.showError( e.getMessage() );
                    }

                    @Override
                    public void onNext( final List<BaseImage> data ) {
                        mDataList.clear();
                        mDataList.addAll( data );
                        mView.updateData( mDataList );
                    }
                });
    }

    @Override
    public void loadMore( final int offset ) {
        if( !mSubscription.isUnsubscribed() ) {
            mSubscription.unsubscribe();
        }
        mSubscription = mModel.getListData( mQuery, String.valueOf( offset ) )
                .flatMap( new Func1<GiphyListResponse, Observable<List<Datum>>>() {
                    @Override
                    public Observable<List<Datum>> call( final GiphyListResponse giphyListResponse ) {
                        return Observable.just( giphyListResponse.getData() );
                    }
                })
                .flatMap( new Func1<List<Datum>, Observable<List<BaseImage>>>() {
                    @Override
                    public Observable<List<BaseImage>> call(final List<Datum> datums) {
                        final List<BaseImage> datas = new ArrayList<BaseImage>(25);
                        for (final Datum datum : datums) {
                            datas.add( datum.getImages().getOriginal() );
                        }
                        return Observable.just( datas );
                    }
                })
                .subscribe(new Observer<List<BaseImage>>() {
                    @Override
                    public void onCompleted() {
                        //do nothing
                    }

                    @Override
                    public void onError(final Throwable e) {
                        mView.showError(e.getMessage());
                    }

                    @Override
                    public void onNext(final List<BaseImage> data) {
                        mDataList.addAll( data );
                        mView.updateData( mDataList );
                    }
                });
    }

    @Override
    public void loadGif( final Context context, final String url, final ImageView view ) {
        // Ion выполняет memoryCache и fileCache
        Ion.with( context )
                .load( url )
                .withBitmap()
                .intoImageView( view );
    }

    @Override
    public void onStop() {
        if ( !mSubscription.isUnsubscribed() ) {
            mSubscription.unsubscribe();
        }
    }


    private boolean isListEmpty() {
        return mDataList == null || mDataList.isEmpty();
    }
}
