package com.example.mailrugroup.presenter;

import android.content.Context;
import android.os.Bundle;
import android.widget.ImageView;

/**
 * Интерфейс для уровня Presenter.
 * Presenter обрабатывает события из View.
 */
public interface IPresenter {

    /**
     * Обработать нажатие кнопки поиска.
     * Во время выполнения очищаются загруженные данные и отображаются только новые.
     */
    public void onLoadClick();

    /**
     * Загрузить еще данных по существующему запросу.
     * @param offset смещение на количество уже загруженных данных.
     */
    public void loadMore( final int offset );

    /**
     * Загрузить gif элемент.
     * Загрузка происходит с помощья сторонних библиотек (Ion).
     * @param context
     * @param url
     * @param view
     */
    public void loadGif( final Context context, final String url, final ImageView view );

    /**
     * Обработчик onCreate для востановления данных.
     * @param bundle
     */
    public void onCreate( final Bundle bundle );

    /**
     * Обработчик onSaveInstanceState для сохранения данных.
     * @param bundle
     */
    public void onSaveInstanceState( final Bundle bundle );

    /**
     * Обработчик onStop для завершения фоновых загрузок, чтобы избежать memory leak.
     */
    public void onStop();
}
