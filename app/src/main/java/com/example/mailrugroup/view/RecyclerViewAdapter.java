package com.example.mailrugroup.view;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.mailrugroup.R;
import com.example.mailrugroup.model.data.BaseImage;

import java.util.ArrayList;
import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    private final IImageLoader mImageLoader;
    /**
     * Цвета для раскрашивание фона элементов.
     */
    private final int[] mColors = { Color.RED, Color.BLUE, Color.GREEN,
            Color.MAGENTA, Color.YELLOW, Color.CYAN };
    private List<BaseImage> mDatas = new ArrayList<BaseImage>();

    public RecyclerViewAdapter( final IImageLoader imageLoader ) {
        mImageLoader = imageLoader;
    }

    @Override
    public ViewHolder onCreateViewHolder( final ViewGroup viewGroup, final int i ) {
        final View view = LayoutInflater.from( viewGroup.getContext() )
                                .inflate(R.layout.gif_layout, viewGroup, false);
        return new ViewHolder( view );
    }

    @Override
    public void onBindViewHolder( final ViewHolder viewHolder, final int i ) {
        final BaseImage gif = mDatas.get( i );
        if( viewHolder.image.getDrawable() == null ) {
            Bitmap bitmap = Bitmap.createBitmap(
                    Integer.valueOf( gif.getWidth() ),
                    Integer.valueOf( gif.getHeight() ),
                    Bitmap.Config.ARGB_8888 );
            bitmap.eraseColor( mColors[ i % mColors.length ] );
            BitmapDrawable ob = new BitmapDrawable( Resources.getSystem(), bitmap );
            if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN ) {
                viewHolder.image.setBackground( ob );
            } else {
                viewHolder.image.setBackgroundDrawable(ob);
            }
            viewHolder.image.postInvalidate();
        }
        mImageLoader.loadImage( gif.getUrl(), viewHolder.image );
    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    public void setData( final List<BaseImage> datas ) {
        mDatas.clear();
        mDatas.addAll( datas );
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView image;

        public ViewHolder( final View itemView ) {
            super( itemView );
            image = (ImageView) itemView.findViewById( R.id.imageView );
        }
    }
}
