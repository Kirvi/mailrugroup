package com.example.mailrugroup.view;

import com.example.mailrugroup.model.data.BaseImage;

import java.util.List;

/**
 * Интерфейс описывающий уровень View.
 */
public interface IView {

    /**
     * Обновить и отобразить данные.
     * @param data новые данные.
     */
    public void updateData( final List<BaseImage> data );

    /**
     * Отобразить Toast с сообщением ошибки.
     * @param message отображаемый текст ошибки.
     */
    public void showError( final String message );

    /**
     * Получить уточняющий запрос для api.giphy.com
     * @return текст запроса.
     */
    public String getQuery();
}
