package com.example.mailrugroup.view;

import android.widget.ImageView;

/**
 * Интерфейс для взаимодействия Adaptera с View.
 */
public interface IImageLoader {
    /**
     * Загрузить изображение.
     * @param url ссылка на изображение.
     * @param view ImageView, в котором необходимо отобразить загружаемое изображение.
     */
    public void loadImage( final String url, final ImageView view );
}
