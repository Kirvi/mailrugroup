package com.example.mailrugroup.view;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.mailrugroup.R;
import com.example.mailrugroup.model.data.BaseImage;
import com.example.mailrugroup.presenter.IPresenter;
import com.example.mailrugroup.presenter.Presenter;

import java.util.List;

public class ListActivity extends AppCompatActivity implements IView, IImageLoader {

    /**
     * Максимальное число элементов в списке. Чтобы избежать OutOfMemory на первых этапах.
     * TODO: Контроль памяти при большом количестве элементов.
     */
    private static final int MAX_ITEMS_COUNT = 100;
    private IPresenter mPresenter;
    private RecyclerView mRecyclerView;
    private RecyclerViewAdapter mAdapter;
    private Toolbar mToolbar;
    private EditText query;

    @Override
    protected void onCreate( final Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_list );

        mPresenter = new Presenter( this );

        setupToolbar();
        setupViews();

        mPresenter.onCreate( savedInstanceState );
    }

    @Override
    public boolean onCreateOptionsMenu( final Menu menu ) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate( R.menu.menu_list, menu );
        return true;
    }

    @Override
    public boolean onOptionsItemSelected( final MenuItem item ) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if( id == R.id.action_search ) {
            mPresenter.onLoadClick();
            return true;
        }

        return super.onOptionsItemSelected( item );
    }

    @Override
    protected void onSaveInstanceState( final Bundle outState ) {
        super.onSaveInstanceState( outState );
        if( mPresenter != null ) {
            mPresenter.onSaveInstanceState( outState );
        }
    }

    @Override
    public void updateData( final List<BaseImage> data ) {
        mAdapter.setData( data );
    }

    @Override
    public void showError( final String message ) {
        Snackbar.make( mToolbar, message, Snackbar.LENGTH_LONG ).show();
    }

    @Override
    public String getQuery() {
        return query.getText().toString();
    }

    @Override
    public void loadImage( final String url, final ImageView view ) {
        mPresenter.loadGif( this, url, view );
    }

    private void setupToolbar() {
        mToolbar = (Toolbar) findViewById( R.id.toolbar );
        mToolbar.setTitle("");
        setSupportActionBar( mToolbar );
    }

    private void setupViews() {
        query = (EditText) findViewById( R.id.editText );
        mRecyclerView = (RecyclerView) findViewById( R.id.recyclerView );
        final StaggeredGridLayoutManager layoutManager =
                new StaggeredGridLayoutManager( 2, 1 );
        layoutManager.setGapStrategy( StaggeredGridLayoutManager.GAP_HANDLING_NONE );
        mRecyclerView.setLayoutManager( layoutManager );
        mAdapter = new RecyclerViewAdapter( this );
        mRecyclerView.setAdapter( mAdapter );
        mRecyclerView.addOnScrollListener( new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled( RecyclerView recyclerView, int dx, int dy )
            {
                if( dy > 0 )
                {
                    final int itemCount =  mAdapter.getItemCount();
                    if( itemCount < MAX_ITEMS_COUNT ) {
                        mPresenter.loadMore( itemCount );
                    }
                }
            }
        });
    }
}
